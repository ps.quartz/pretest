package com.ascendcorp.exam.util;

public enum ResponseCode {
	
	code200("200",""),
	code400("400","General Invalid Data"),
	code500("500","General Transaction Error"),
	code501("501","General Invalid Data"),
	code503("503","Error timeout"),
	code504("504","Internal Application Error");
	
	private final String reasonCode;
	private final String reasonCodeDesc;
	
	private ResponseCode(String reasonCode, String reasonCodeDesc) {
		this.reasonCode = reasonCode;
		this.reasonCodeDesc = reasonCodeDesc;
	}

	public String getReasonCode() {
		return reasonCode;
	}

	public String getReasonCodeDesc() {
		return reasonCodeDesc;
	}

	
	
	
	
		

}
