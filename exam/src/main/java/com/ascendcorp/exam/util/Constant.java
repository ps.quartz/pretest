package com.ascendcorp.exam.util;

public class Constant {
	
	public class Status {
		public static final String approved = "approved";
		public static final String invalidData = "invalid_data";
		public static final String transactionError ="transaction_error";
		public static final String unknown ="unknown";
	}
	
	
	public class Exception {
		public static final String socket ="java.net.SocketTimeoutException";
		public static final String timeOut = "Connection timed out";
	}
}
